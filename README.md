# NVIR preview node

## Get Started

```shell
npm install --save https://gitlab.com/nilvana-ai/edge/nvir-preview-node.git
```

## Remove node

```
npm remove 12-nilvana-inference-runtime-recognition-nvir-image-preview
```

## Copyright and license

Copyright inwinSTACK Inc. under [the Apache 2.0 license](LICENSE.md).
